package gossip

// import (
// 	"fmt"
// )

const (
	ID_SEPARATOR = "|"
)

// // View implements Viewer to store any Describer interface
// type View map[string]Describer

// func NewView(items ...Describer) (Viewer, error) {
// 	var v View = make(map[string]Describer)
// 	if err := v.Add(items...); err != nil {
// 		return nil, err
// 	}
// 	return &v, nil
// }

// func UnionView(views ...Viewer) (Viewer, error) {
// 	v, _ := NewView()
// 	for _, view := range views {
// 		if view == nil {
// 			continue
// 		}
// 		if err := v.Add(view.List()...); err != nil {
// 			return nil, err
// 		}
// 	}

// 	return v, nil
// }

// func (v *View) Add(items ...Describer) error {
// 	for _, item := range items {
// 		if item == nil {
// 			continue
// 		}
// 		// Replace potential duplicates
// 		(*v)[item.ID()] = item.Copy()
// 	}
// 	return nil
// }
// func (v *View) Copy() Viewer {
// 	var v2 View = make(map[string]Describer)
// 	for key, item := range *v {
// 		v2[key] = item.Copy()
// 	}
// 	return &v2
// }
// func (v *View) Diff(v2 Viewer) {
// 	if v2 == nil {
// 		return
// 	}
// 	for key2 := range v2.Map() {
// 		delete(*v, key2)
// 	}
// }
// func (v *View) Empty() bool {
// 	return len(*v) == 0
// }

// func (v *View) Equals(v2 Viewer) bool {
// 	if v2 == nil || len(*v) != v2.Len() {
// 		return false
// 	}
// 	for _, item := range *v {
// 		if item2, ok := v2.Map()[item.ID()]; ok {
// 			if !item.Equals(item2) {
// 				return false
// 			}
// 		} else {
// 			return false
// 		}
// 	}
// 	return true
// }
// func (v *View) Get(key string) Describer {
// 	if d, ok := (*v)[key]; !ok {
// 		return nil
// 	} else {
// 		return d.Copy()
// 	}
// }
// func (v *View) GetSubset(length int) Viewer {
// 	if v.Len() < length {
// 		return v.Copy()
// 	}

// 	v2, _ := NewView(v.List()[:length]...)
// 	return v2
// }
// func (v *View) Has(item Describer) bool {
// 	_, ok := (*v)[item.ID()]
// 	return ok
// }
// func (v *View) IDs() string {
// 	ret := ""
// 	i := 0
// 	for key := range *v {
// 		ret += key
// 		if i < len(*v)-1 {
// 			ret += ID_SEPARATOR
// 		}
// 		i++
// 	}
// 	return ret
// }
// func (v *View) Len() int {
// 	return len(*v)
// }
// func (v *View) List() []Describer {
// 	list := make([]Describer, v.Len())
// 	i := 0
// 	for _, item := range *v {
// 		list[i] = item.Copy()
// 		i++
// 	}
// 	return list
// }
// func (v *View) Map() map[string]Describer {
// 	newMap := make(map[string]Describer)
// 	for id, item := range *v {
// 		newMap[id] = item.Copy()
// 	}
// 	return newMap
// }

// // func (v *View) Merge(views ...Viewer) error {
// // 	for _, view := range views {
// // 		if view == nil {
// // 			continue
// // 		}
// // 		if err := v.Add(view.List()...); err != nil {
// // 			return err
// // 		}
// // 	}
// // 	return nil
// // }
// func (v *View) Remove(items ...Describer) {
// 	for _, item := range items {
// 		if item != nil {
// 			delete(*v, item.ID())
// 		}

// 	}
// }
// func (v *View) String() string {
// 	ret := fmt.Sprintf("View(len:%v){", len(*v))
// 	for _, item := range *v {
// 		ret += fmt.Sprintf("\n\t%s,", item)
// 	}
// 	ret += "}"
// 	return ret
// }

// func (v *View) GetOldest() Describer {
// 	oldestAge := 0
// 	oldestKey := ""
// 	for key, item := range *v {
// 		if item.Age() >= oldestAge {
// 			oldestKey = key
// 			oldestAge = item.Age()
// 		}
// 	}
// 	return (*v)[oldestKey].Copy()
// }
// func (v *View) IncrementAge() {
// 	for _, item := range *v {
// 		item.IncrementAge()
// 	}
// }
// func (v *View) ResetAge() {
// 	for key := range *v {
// 		(*v)[key] = (*v)[key].ResetAge()
// 	}
// }
