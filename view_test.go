package gossip

// import (
// 	"fmt"
// 	"testing"
// )

// var descriptors []Describer = []Describer{
// 	&Descriptor{"a", "192.168.1.1"},
// 	&Descriptor{"b", "192.168.1.2"},
// 	&Descriptor{"c", "192.168.1.3"},
// 	&Descriptor{"d", "192.168.1.4"},
// 	&Descriptor{"e", "192.168.1.5"},
// 	&Descriptor{"f", "192.168.1.6"},
// 	&Descriptor{"g", "192.168.1.7"},
// 	&Descriptor{"h", "192.168.1.8"},
// 	&Descriptor{"i", "192.168.1.9"},
// 	&Descriptor{"j", "192.168.1.10"},
// }

// // June 1st 18: untestes: Map(), List()

// // Utility function
// func viewEqualsList(V Viewer, descriptors []Describer) error {
// 	var ok bool
// 	var v *View
// 	if v, ok = V.(*View); !ok {
// 		return fmt.Errorf("Viewer is not a *cyclon.View.\n")
// 	}

// 	if len(*v) != len(descriptors) {
// 		return fmt.Errorf("View and descriptors are not the same length:\nV: %v\ndescriptors: %v\n",
// 			v, descriptors)
// 	}

// 	for _, desc := range descriptors {
// 		found := false
// 		for key, item := range *v {
// 			if key == desc.ID() && item.Equals(desc) {
// 				found = true
// 				break
// 			}
// 		}

// 		if !found {
// 			return fmt.Errorf("v does not contain descriptor %v:\nV: %v\ndescriptors: %v\n",
// 				desc, v, descriptors)
// 		}
// 	}
// 	return nil
// }

// func TestViewNewView(t *testing.T) {
// 	V, err := NewView(descriptors...)

// 	if err != nil {
// 		t.Errorf("TestViewNewView failed: %v", err)
// 	}

// 	if err = viewEqualsList(V, descriptors); err != nil {
// 		t.Errorf("TestViewNewView failed: %v", err)
// 	}
// }

// func TestViewUnionViewDistinct(t *testing.T) {
// 	V1, _ := NewView(descriptors[:5]...)
// 	V2, _ := NewView(descriptors[5:]...)
// 	V, err := UnionView(V1, V2)

// 	if err != nil {
// 		t.Errorf("TestViewUnionViewDistinct failed: %v", err)
// 	}

// 	if err = viewEqualsList(V, descriptors); err != nil {
// 		t.Errorf("TestViewUnionViewDistinct failed: %v", err)
// 	}
// }
// func TestViewUnionViewOverlapping(t *testing.T) {
// 	V1, _ := NewView(descriptors[:7]...)
// 	V2, _ := NewView(descriptors[5:]...)
// 	V, err := UnionView(V1, V2)

// 	if err != nil {
// 		t.Errorf("TestViewUnionViewOverlapping failed: %v", err)
// 	}

// 	if err = viewEqualsList(V, descriptors); err != nil {
// 		t.Errorf("TestViewUnionViewOverlapping failed: %v", err)
// 	}
// }

// func TestViewAddNewDesc(t *testing.T) {
// 	V, _ := NewView(descriptors[:5]...)
// 	err := V.Add(descriptors[5])

// 	if err != nil {
// 		t.Errorf("TestViewAddNewDesc failed: %v", err)
// 	}

// 	if err = viewEqualsList(V, descriptors[:6]); err != nil {
// 		t.Errorf("TestViewAddNewDesc failed: %v", err)
// 	}
// }
// func TestViewAddExistingDesc(t *testing.T) {
// 	V, _ := NewView(descriptors[:5]...)
// 	err := V.Add(descriptors[0])

// 	if err != nil {
// 		t.Errorf("TestViewAddExistingDesc failed: %v", err)
// 	}

// 	if err := viewEqualsList(V, descriptors[:5]); err != nil {
// 		t.Errorf("TestViewAddExistingDesc failed: %v", err)
// 	}
// }

// func TestViewCopy(t *testing.T) {
// 	V, _ := NewView(descriptors...)
// 	Vcopy := V.Copy()

// 	if err := viewEqualsList(V, descriptors); err != nil {
// 		t.Errorf("TestViewCopy failed with V: %v", err)
// 	}

// 	if err := viewEqualsList(Vcopy, descriptors); err != nil {
// 		t.Errorf("TestViewCopy failed with Vcopy: %v", err)
// 	}

// 	// Compare addresses
// 	if V == Vcopy {
// 		t.Errorf("TestViewCopy failed: both views have the same address:V: %p, Vcopy: %p\n",
// 			V, Vcopy)
// 	}

// 	for _, item := range descriptors {
// 		if (*V)[item.ID()] == (*Vcopy.(*View))[item.ID()] {
// 			t.Errorf(
// 				"TestViewCopy failed: descriptor %v (@ %p) has the same address in both views:V.",
// 				item, item)
// 		}
// 	}
// }

// func TestViewDiff(t *testing.T) {
// 	V, _ := NewView(descriptors[:8]...)
// 	V2, _ := NewView(descriptors[5:]...)
// 	V.Diff(V2)

// 	if err := viewEqualsList(V, descriptors[:5]); err != nil {
// 		t.Errorf("TestViewDiff failed: %v", err)
// 	}
// }

// func TestViewEmpty(t *testing.T) {
// 	V, _ := NewView()

// 	if !V.Empty() {
// 		t.Errorf("TestViewEmpty failed: V not empty:\nV: %v\n", V)
// 	}
// }

// func TestViewEqualsSelf(t *testing.T) {
// 	V, _ := NewView(descriptors...)

// 	if !V.Equals(V) {
// 		t.Errorf("TestViewEqualsSelf failed: V does not equal V:\nV: %v\n", V)
// 	}
// }

// func TestViewEqualsOther(t *testing.T) {
// 	V, _ := NewView(descriptors...)
// 	V2, _ := NewView(descriptors...)

// 	if !V.Equals(V2) {
// 		t.Errorf("TestViewEqualsOther failed: V does not equal V2:\nV: %v\nV2: %v\n",
// 			V, V2)
// 	}
// }

// func TestViewEqualsCopy(t *testing.T) {
// 	V, _ := NewView(descriptors...)
// 	V2 := V.Copy()

// 	if !V.Equals(V2) {
// 		t.Errorf("TestViewEqualsCopy failed: V does not equal V2:\nV: %v\nV2: %v\n",
// 			V, V2)
// 	}
// }

// func TestViewHas(t *testing.T) {
// 	V, _ := NewView(descriptors...)

// 	if !V.Has(descriptors[0]) {
// 		t.Errorf("TestViewHas failed:\nV: %v\n query: %v\n",
// 			V, descriptors[0])
// 	}
// }
// func TestViewHasNot(t *testing.T) {
// 	V, _ := NewView(descriptors[1:]...)

// 	if V.Has(descriptors[0]) {
// 		t.Errorf("TestViewHasNot failed:\nV: %v\n query: %v\n",
// 			V, descriptors[0])
// 	}
// }

// func TestViewLen(t *testing.T) {
// 	V, _ := NewView(descriptors...)

// 	if V.Len() != len(descriptors) {
// 		t.Errorf("TestViewLen failed:\nV: %v\ndescriptors: %v\n", V, descriptors)
// 	}
// }

// // func TestViewMergeDistinct(t *testing.T) {
// // 	V, _ := NewView(descriptors[:5]...)
// // 	V2, _ := NewView(descriptors[5:]...)
// // 	err := V.Merge(V2)

// // 	if err != nil {
// // 		t.Errorf("TestMergeDistinct failed: %v", err)
// // 	}

// // 	if err = viewEqualsList(V, descriptors); err != nil {
// // 		t.Errorf("TestViewMergeDistinct failed: %v", err)
// // 	}
// // }

// // func TestViewMergeOverlapping(t *testing.T) {
// // 	V, _ := NewView(descriptors[:5]...)
// // 	V2, _ := NewView(descriptors[2:8]...)
// // 	err := V.Merge(V2)

// // 	if err != nil {
// // 		t.Errorf("TestViewMergeOverlapping failed: %v", err)
// // 	}

// // 	if err = viewEqualsList(V, descriptors[:8]); err != nil {
// // 		t.Errorf("TestViewMergeOverlapping failed: %v", err)
// // 	}
// // }

// func TestViewRemoveExisting(t *testing.T) {
// 	V, _ := NewView(descriptors...)
// 	V.Remove(descriptors[5:]...)

// 	if err := viewEqualsList(V, descriptors[:5]); err != nil {
// 		t.Errorf("TestViewRemoveExisting failed: %v\n", err)
// 	}
// }
// func TestViewRemoveNotExisting(t *testing.T) {
// 	V, _ := NewView(descriptors[:5]...)
// 	V.Remove(descriptors[5:]...)

// 	if err := viewEqualsList(V, descriptors[:5]); err != nil {
// 		t.Errorf("TestViewRemoveNotExisting failed: %v\n", err)
// 	}
// }
// func TestViewRemovePartlyExisting(t *testing.T) {
// 	V, _ := NewView(descriptors[:8]...)
// 	V.Remove(descriptors[5:]...)

// 	if err := viewEqualsList(V, descriptors[:5]); err != nil {
// 		t.Errorf("TestViewRemovePartlyExisting failed: %v\n", err)
// 	}
// }
