package storyteller

import (
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/transport"
	"time"
)

type Payload struct {
	Sender gossip.Describer
	S      map[time.Time]Item
	ID     int32
}

type ActivityPayload struct {
	Sender    gossip.Describer
	S         map[time.Time]Item
	MessageID int32
}

type AckPayload struct {
	Sender    gossip.Describer
	Type      transport.MessageType
	MessageID int32
}
