package storyteller

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
	"os"
	"sort"
	"sync"
	"time"
)

// Item is the interface going inside Sequence
type Item interface {
	Descriptor() gossip.Describer
	Timestamp() time.Time

	Equals(Item) bool
}

type Sequence struct {
	sync.RWMutex
	m map[time.Time]Item
}

func NewSequence(its ...Item) *Sequence {
	var S Sequence
	S.m = make(map[time.Time]Item)
	S.Add(its...)
	return &S
}

func SequenceDiff(S1, S2 *Sequence) *Sequence {
	S := S1.Copy()

	S2.RLock()
	defer S2.RUnlock()

	for key2 := range S2.m {
		delete(S.m, key2)
	}

	return S
}

func (S *Sequence) Add(its ...Item) {
	S.Lock()
	defer S.Unlock()
	for _, it := range its {
		S.m[it.Timestamp()] = it
	}
}
func (S *Sequence) Copy() *Sequence {
	S.RLock()
	defer S.RUnlock()
	var S2 Sequence
	S2.m = make(map[time.Time]Item)
	for key, item := range S.m {
		S2.m[key] = item
	}

	return &S2
}
func (S *Sequence) Equals(S2 *Sequence) bool {
	S.RLock()
	defer S.RUnlock()

	S2.RLock()
	defer S2.RUnlock()

	if S.Len() != S2.Len() {
		return false
	}
	for _, it := range S.m {
		if !S2.Has(it) || !it.Equals(S2.Get(it.Timestamp())) {
			return false
		}
	}

	return true
}

func (S *Sequence) Get(t time.Time) Item {
	S.RLock()
	defer S.RUnlock()

	if it, ok := S.m[t]; !ok {
		return nil
	} else {
		return it
	}
}

func (S *Sequence) GetDescriptors(
	exclude ...gossip.Describer) (descsArr []gossip.Describer) {
	// Little linear search function
	isInArray := func(arr []gossip.Describer, desc gossip.Describer) bool {
		for _, d := range arr {
			if d == nil {
				continue
			}
			if desc.Equals(d) {
				return true
			}
		}
		return false
	}

	S.RLock()
	defer S.RUnlock()
	for _, it := range S.m {
		// Continue if it.D was already in the descsMap
		if isInArray(descsArr, it.Descriptor()) {
			continue
		}

		// Add it.D in descsArr only if not in exclude
		if !isInArray(exclude, it.Descriptor()) {
			descsArr = append(descsArr, it.Descriptor())
		}
	}

	return
}

func (S *Sequence) Has(it Item) bool {
	_, ok := S.m[it.Timestamp()]
	return ok
}

func (S *Sequence) Last() Item {
	sortedKeys := S.SortedKeys()

	return S.m[sortedKeys[len(sortedKeys)-1]]
}

func (S *Sequence) Len() int {
	return len(S.m)
}

// Returns the diff and merges in-place
func (S *Sequence) Merge(S2 *Sequence) []Item {
	diffSeq := make([]Item, 0, len(S2.m))

	S.Lock()
	defer S.Unlock()

	S2.RLock()
	defer S2.RUnlock()

	for k, v := range S2.m {
		if it, ok := S.m[k]; ok && !it.Equals(v) {
			fmt.Fprintf(os.Stderr, "[Sequence.Merge] Sequence has different element at key %q:\n"+
				"\tIn S: %q;\n\tIn S2: %q\n", k, it, v)
		} else if !ok {
			S.m[k] = v
			diffSeq = append(diffSeq, v)
		}
	}

	return diffSeq
}

func (S *Sequence) Remove(its ...Item) {
	S.Lock()
	defer S.Unlock()

	for _, it := range its {
		delete(S.m, it.Timestamp())
	}
}

func (S *Sequence) SortedKeys() []time.Time {
	S.RLock()
	defer S.RUnlock()

	keys := []time.Time{}
	for k := range S.m {
		// index out of range happened here once...
		keys = append(keys, k)
	}

	sort.Sort(timeSlice(keys))
	return keys
}

func (S *Sequence) String() string {
	ret := "Sequence{\n"

	S.RLock()
	defer S.RUnlock()
	for _, t := range S.SortedKeys() {
		ret += fmt.Sprintf("\t%v\n", S.m[t])
	}
	return ret + "}"
}

func (S *Sequence) PrintTimestamps() (ret string) {
	timestamps := S.SortedKeys()
	for i, k := range timestamps {
		ret += k.Format(gossip.TIME_LAYOUT)
		if i != len(timestamps)-1 {
			ret += gossip.ID_SEPARATOR
		}
	}
	return
}

// TODO: GetDescriptors

// Sort internal bullshit
type timeSlice []time.Time

func (t timeSlice) Len() int           { return len(t) }
func (t timeSlice) Less(i, j int) bool { return t[i].Before(t[j]) }
func (t timeSlice) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }

// type Sequence map[time.Time]Interaction

// func NewSequence(its ...Interaction) *Sequence {
// 	var S Sequence = make(map[time.Time]Interaction)
// 	S.Add(its...)
// 	return &S
// }

// func (S *Sequence) Add(its ...Interaction) {
// 	for _, it := range its {
// 		(*S)[it.Timestamp] = it
// 	}
// }
// func (S *Sequence) Copy() *Sequence {
// 	var S2 Sequence = make(map[time.Time]Interaction)
// 	for key, item := range *S {
// 		S2[key] = item
// 	}
// 	return &S2
// }
// func (S *Sequence) Equals(S2 *Sequence) bool {
// 	if len(*S) != len(*S2) {
// 		return false
// 	}
// 	for _, it := range *S {
// 		if !S2.Has(it) || !it.Equals(S2.GetInteraction(it.Timestamp)) {
// 			return false
// 		}
// 	}

// 	return true
// }

// // Unused?
// // func (S *Sequence) Get(t time.Time) (Interaction, bool) {
// // 	return (*S)[t]
// // }

// func (S *Sequence) GetDescriptors(
// 	exclude ...gossip.Describer) (descsArr []gossip.Describer) {
// 	// Little linear search function
// 	isInArray := func(arr []gossip.Describer, desc gossip.Describer) bool {
// 		for _, d := range arr {
// 			if d == nil {
// 				continue
// 			}
// 			if desc.Equals(d) {
// 				return true
// 			}
// 		}
// 		return false
// 	}

// 	for _, it := range *S {
// 		// Continue if it.D was already in the descsMap
// 		if isInArray(descsArr, it.D) {
// 			continue
// 		}

// 		// Add it.D in descsArr only if not in exclude
// 		if !isInArray(exclude, it.D) {
// 			descsArr = append(descsArr, it.D)
// 		}
// 	}

// 	return
// }

// func (S *Sequence) GetInteraction(t time.Time) Interaction {
// 	return (*S)[t]
// }

// func (S *Sequence) GetLastInteraction() Interaction {
// 	sortedKeys := S.GetSortedKeys()
// 	lastTimestamp := sortedKeys[len(sortedKeys)-1]

// 	return (*S)[lastTimestamp]
// }

// func (S *Sequence) GetSortedKeys() []time.Time {
// 	keys := make([]time.Time, len(*S))
// 	i := 0
// 	for k := range *S {
// 		keys[i] = k
// 		i++
// 	}

// 	sort.Sort(timeSlice(keys))
// 	return keys

// }
// func (S *Sequence) Has(it Interaction) bool {
// 	_, ok := (*S)[it.Timestamp]
// 	return ok
// }
// func (S *Sequence) Len() int {
// 	return len(*S)
// }
// func (S *Sequence) Merge(S2 *Sequence) bool {
// 	updated := false
// 	for k, v := range *S2 {
// 		if it, ok := (*S)[k]; ok && !it.Equals(v) {
// 			fmt.Fprintf(os.Stderr, "[Sequence.Merge] Sequence has different element at key %q:\n"+
// 				"\tIn S: %q;\n\tIn S2: %q\n", k, it, v)
// 		} else if !ok {
// 			(*S)[k] = v
// 			updated = true
// 		}
// 	}

// 	return updated
// }
// func (S *Sequence) Remove(its ...Interaction) {
// 	for _, it := range its {
// 		delete(*S, it.Timestamp)
// 	}
// }
// func (S *Sequence) String() string {
// 	ret := "Sequence{\n"
// 	for _, t := range S.GetSortedKeys() {
// 		ret += fmt.Sprintf("%v\n", (*S)[t])
// 	}
// 	return ret + "}"
// }
// func (S *Sequence) PrintTimestamps() (ret string) {
// 	timestamps := S.GetSortedKeys()
// 	for i, k := range timestamps {
// 		ret += k.Format(gossip.TIME_LAYOUT)
// 		if i != len(timestamps)-1 {
// 			ret += gossip.ID_SEPARATOR
// 		}
// 	}

// 	return
// }

// func SequenceDiff(S1, S2 *Sequence) *Sequence {
// 	S := S1.Copy()
// 	for key2 := range *S2 {
// 		delete(*S, key2)
// 	}

// 	return S
// }

// // Sort internal bullshit
// type timeSlice []time.Time

// func (t timeSlice) Len() int           { return len(t) }
// func (t timeSlice) Less(i, j int) bool { return t[i].Before(t[j]) }
// func (t timeSlice) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }
