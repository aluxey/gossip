package storyteller

import (
	"encoding/gob"
	"fmt"
	"gitlab.inria.fr/aluxey/csvlog"
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/transport"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	ACK_TIMEOUT = 200 // ms
	// defined in gossip
	//TIME_LAYOUT = time.RFC3339Nano
)

func init() {
	gob.Register(Payload{})
}

type StoryTeller struct {
	myself gossip.Describer

	// My sequence, and the initial sequence given on bootstrap
	sequence, initialSequence *Sequence
	// Our knowledge of our peers' sequences
	otherSequences struct {
		sync.RWMutex
		m map[string]*Sequence
	}

	// Number of peers to which we communicate our sequence updates
	fanout int

	// Network
	tm                  transport.Manager
	reqCh, ansCh, ackCh <-chan transport.Message

	// Acknowledgement map
	ackMap struct {
		sync.RWMutex
		m map[int32]chan struct{}
	}

	interactionReceviers []chan<- Item

	// Logging
	csv         *csvlog.CSVLog
	info, debug *log.Logger
	// count #messages and # useless messages received
	nMessages, nUselessMessages int

	mutex sync.RWMutex
}

func New(myself gossip.Describer, initialSequence *Sequence,
	tm transport.Manager,
	fanout int, sequencePath string,
	logToCsv, debug bool) (*StoryTeller, error) {

	st := new(StoryTeller)

	st.fanout = fanout
	st.myself = myself

	// ---------- Sequences & Descriptor ----------
	st.initialSequence = initialSequence
	st.sequence = st.initialSequence.Copy()

	// Initialise the otherSequences map:
	// for each descriptor in our S, we consider that each other node knows the same sequence
	st.otherSequences.m = make(map[string]*Sequence)
	for _, d := range st.sequence.GetDescriptors(st.myself) {
		st.otherSequences.m[d.ID()] = st.initialSequence.Copy()
	}

	// ---------- Network ----------
	st.tm = tm
	// Creation of the chans
	reqCh := make(chan transport.Message)
	ansCh := make(chan transport.Message)
	ackCh := make(chan transport.Message)
	// Register chans on the network Manager
	st.tm.RegisterObserver(transport.MessageType("ST_REQ"), reqCh)
	st.tm.RegisterObserver(transport.MessageType("ST_ANS"), ansCh)
	st.tm.RegisterObserver(transport.MessageType("ST_ACK"), ackCh)
	// Save receive chan in st object
	st.reqCh = reqCh
	st.ansCh = ansCh
	st.ackCh = ackCh

	// Acknowledgement map
	st.ackMap.m = make(map[int32]chan struct{})

	// ---------- Logging ----------
	// To console
	st.info = log.New(os.Stdout, "STORYTELLER: ", log.Ltime|log.Lshortfile)
	if debug {
		st.debug = log.New(os.Stdout, "STORYTELLER: ", log.Ltime|log.Lshortfile)
	} else {
		st.debug = log.New(ioutil.Discard, "STORYTELLER: ", log.Ltime|log.Lshortfile)
	}
	// To CSV
	if logToCsv {
		now := time.Now()
		filename := fmt.Sprintf("storyteller-%v_%02d-%02d-%02d_%02dh%02d.csv",
			st.myself.ID(),
			now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
		header := []string{"id", "ip", "timestamp", "sequenceTimestamps",
			"updateSource", "nMessagesReceived",
			"nUselessMessagesReceived"}

		var err error
		if st.csv, err = csvlog.New(filename, header); err != nil {
			return nil, fmt.Errorf("Failed creating StoryTeller CSVLog: %v", err)
		}
	}

	// ---------- Interaction receivers ----------
	st.interactionReceviers = make([]chan<- Item, 0)

	st.info.Printf("StoryTeller initialized with fanout=%d.\n", st.fanout)

	return st, nil
}

func (st *StoryTeller) AddInteractionReceiver(receiver chan<- Item) {
	st.interactionReceviers = append(st.interactionReceviers, receiver)
}

func (st *StoryTeller) NotifyInteractionReceivers(items ...Item) {
	for _, it := range items {
		for i, ch := range st.interactionReceviers {
			go func(ch chan<- Item, it Item) {
				ch <- it
				st.debug.Printf("Successfully notified receiver #%d of interaction: %v\n", i, it)
			}(ch, it)
		}
	}
}

func (st *StoryTeller) ReceiveDaemon() {
	st.info.Printf("ReceiveDaemon started.\n")

	for {
		select {
		case mess := <-st.reqCh:
			go st.handleRequest(mess)
		case mess := <-st.ansCh:
			go st.handleAnswer(mess)
		case mess := <-st.ackCh:
			go st.dispatchAck(mess)
		}
	}
}

func (st *StoryTeller) AddNewInteraction(it Item) *Sequence {
	st.mutex.Lock()
	defer st.mutex.Unlock()

	st.sequence.Add(it)

	st.info.Printf("User performed a new interaction:\n%v\n\n", it)

	// Notify receivers
	st.NotifyInteractionReceivers(it)

	go st.addLogLine(it.Timestamp(), st.myself)
	go st.gossipUpdate(nil)

	return st.sequence.Copy()
}

func (st *StoryTeller) Sequence() *Sequence {
	st.mutex.RLock()
	defer st.mutex.RUnlock()
	return st.sequence.Copy()
}

func (st *StoryTeller) gossipUpdate(excluded gossip.Describer) {
	st.mutex.RLock()
	sequence := st.sequence.Copy()
	st.mutex.RUnlock()

	// We will communicate with nodes from our sequence
	// excluding myself, the device used in the last interaction,
	// and the optional 'excluded' peer
	peers := selectRandomPeers(sequence, st.fanout, false,
		st.myself,
		sequence.Last().Descriptor(),
		excluded)

	st.debug.Printf("We will gossip the new interaction with:\n%v\n",
		peers)

	for _, peer := range peers {
		st.otherSequences.Lock()
		if _, ok := st.otherSequences.m[peer.ID()]; !ok {
			st.otherSequences.m[peer.ID()] = st.initialSequence.Copy()
		}

		// diffSeq contains the its we believe peer does not know of among our sequence:
		// diffSeq = our Sequence \ peer's local Sequence
		diffSeq := SequenceDiff(sequence, st.otherSequences.m[peer.ID()])
		st.otherSequences.Unlock()

		// We only communicate with peer if we have something for it
		if diffSeq.Len() > 0 {
			// Communication inside goroutine: parallelize sending requests
			go func(st *StoryTeller, peer gossip.Describer, diffSeq *Sequence) {
				st.debug.Printf("Sending the following REQUEST to %v:\n%v\n",
					peer, diffSeq)

				// Message has a unique messageID
				req := Payload{st.myself, diffSeq.m, rand.Int31()}

				transport.EncodeAndSend(
					req,
					peer.IP(),
					st.tm,
					transport.MessageType("ST_REQ"))

				// Add it to the ack map
				st.ackMap.Lock()
				ch := make(chan struct{})
				st.ackMap.m[req.ID] = ch
				st.ackMap.Unlock()

				select {
				// We have received the Acknowledgement
				case <-ch:
					// Update our local view of peer's sequence
					st.debug.Printf(
						"Ack from %v for our REQUEST received: merging its remote view.\n",
						peer)
					st.otherSequences.Lock()
					st.otherSequences.m[peer.ID()].Merge(diffSeq)
					st.otherSequences.Unlock()
				// It timed out
				case <-time.After(ACK_TIMEOUT * time.Millisecond):
					st.debug.Printf("Ack from %v for our REQUEST timed out.\n",
						peer)
				}
			}(st, peer, diffSeq)

		} else {
			st.debug.Printf("%v seems up to date with our sequence: skpping it.\n",
				peer)
		}
	}
}

func (st *StoryTeller) handleRequest(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("ST_REQ"))
	if x == nil {
		st.info.Printf(
			"Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)
	recSeq := NewSequence()
	recSeq.m = payload.S

	st.debug.Printf("Received the following REQUEST from %v:\n%v\n\n",
		payload.Sender, recSeq)

	// First sending acknowledgement to the remote
	// We set the payload Sequence to nil: no need
	go transport.EncodeAndSend(
		Payload{st.myself, nil, payload.ID},
		payload.Sender.IP(), st.tm, transport.MessageType("ST_ACK"))

	// Locking the otherSequences mutex while we use it
	st.otherSequences.Lock()
	if _, ok := st.otherSequences.m[payload.Sender.ID()]; !ok {
		st.otherSequences.m[payload.Sender.ID()] = st.initialSequence.Copy()
	}
	// We update our local version of the remote's sequence with what it sent
	st.otherSequences.m[payload.Sender.ID()].Merge(recSeq)

	itsSeq := st.otherSequences.m[payload.Sender.ID()].Copy()
	st.otherSequences.Unlock()

	// Locking the st Mutex while we modify/access the sequence
	st.mutex.Lock()

	// We update our sequence with the received payload
	newInteractions := st.sequence.Merge(recSeq)
	if len(newInteractions) > 0 {
		st.NotifyInteractionReceivers(newInteractions...)
	}

	// Just a little logging
	if st.csv != nil {
		// Incrementing messages counters
		st.nMessages++
		if len(newInteractions) == 0 {
			st.nUselessMessages++
		} else {
			// If my sequence was updated, add a csv line
			go st.addLogLine(time.Now(), payload.Sender)
		}
	}

	// diffSeq contains the interactions known by us and not by the remote
	diffSeq := SequenceDiff(st.sequence, itsSeq)

	st.mutex.Unlock()

	// Only answer to the remote if we have interactions to send it
	if diffSeq.Len() > 0 {
		st.debug.Printf("Sending the following ANSWER to %v:\n%v\n\n",
			payload.Sender, diffSeq)

		ans := Payload{st.myself, diffSeq.m, rand.Int31()}

		go transport.EncodeAndSend(
			ans,
			payload.Sender.IP(),
			st.tm,
			transport.MessageType("ST_ANS"))

		st.ackMap.Lock()
		ch := make(chan struct{})
		st.ackMap.m[ans.ID] = ch
		st.ackMap.Unlock()

		select {
		// We have received the Acknowledgement
		case <-ch:
			// Update our local view of peer's sequence
			st.debug.Printf("Ack from %v for our ANSWER received: merging its remote view.\n",
				payload.Sender)
			st.otherSequences.Lock()
			st.otherSequences.m[payload.Sender.ID()].Merge(diffSeq)
			st.otherSequences.Unlock()
		// It timed out
		case <-time.After(ACK_TIMEOUT * time.Millisecond):
			st.debug.Printf("Ack from %v for our ANSWER timed out.\n",
				payload.Sender)
		}

	} else {
		st.debug.Printf("Nothing to answer to %v, we're up to date!\n\n",
			payload.Sender)
	}

	// If my sequence was updated with this request, we forward the update to other peers
	// Excluding the remote that sent us the update
	if len(newInteractions) > 0 {
		st.gossipUpdate(payload.Sender)
	}
}

func (st *StoryTeller) handleAnswer(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("ST_ANS"))
	if x == nil {
		st.info.Printf(
			"Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)
	recSeq := NewSequence()
	recSeq.m = payload.S

	st.debug.Printf("Received the following ANSWER from %v:\n%v\n\n",
		payload.Sender, recSeq)

	// First sending acknowledgement to the remote
	go transport.EncodeAndSend(
		Payload{st.myself, nil, payload.ID},
		payload.Sender.IP(), st.tm, transport.MessageType("ST_ACK"))

	// We update our local version of the remote's sequence with what it sent
	st.otherSequences.Lock()
	if _, ok := st.otherSequences.m[payload.Sender.ID()]; !ok {
		st.otherSequences.m[payload.Sender.ID()] = st.initialSequence.Copy()
	}
	st.otherSequences.m[payload.Sender.ID()].Merge(recSeq)
	st.otherSequences.Unlock()

	// Locking the mutex while we modify attributes
	st.mutex.Lock()
	defer st.mutex.Unlock()

	// We update our sequence with the received payload
	newInteractions := st.sequence.Merge(recSeq)
	if len(newInteractions) > 0 {
		st.NotifyInteractionReceivers(newInteractions...)
	}

	if st.csv != nil {
		st.nMessages++
		if len(newInteractions) == 0 {
			st.nUselessMessages++
		} else {
			// If my sequence was updated and we log, add a csv line
			go st.addLogLine(time.Now(), payload.Sender)
		}
	}
}

func (st *StoryTeller) dispatchAck(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("ST_ACK"))
	if x == nil {
		st.info.Printf(
			"Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	st.ackMap.RLock()
	defer st.ackMap.RUnlock()
	if ch, ok := st.ackMap.m[payload.ID]; ok {
		// Closing the chan will unlock goroutines waiting on it
		close(ch)
	} else {
		st.info.Printf("Received unexpected ACK: %v.\n",
			payload)
	}
}

// Log a new line to the csvlog
func (st *StoryTeller) addLogLine(ts time.Time, updateSource gossip.Describer) {
	if st.csv != nil {

		nMessages, nUselessMessages := st.nMessages, st.nUselessMessages
		st.nMessages, st.nUselessMessages = 0, 0

		// header: {"id", "ip", "timestamp", "sequenceTimestamps",
		// "updateSource", "nMessagesReceived",
		// "nUselessMessagesReceived"}

		st.csv.Write([]string{
			st.myself.ID(),
			st.myself.IP(),
			// Python recognizes RFC3339Nano well, and it's high-precision
			// For instance one can use pandas.to_datetime(...) to parse it
			ts.Format(gossip.TIME_LAYOUT),
			st.sequence.PrintTimestamps(),
			updateSource.ID(),
			strconv.Itoa(nMessages),        // number of received messages since last line
			strconv.Itoa(nUselessMessages), // number of received useless messages since last line
		})
	}
}
