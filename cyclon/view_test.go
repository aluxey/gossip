package cyclon

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
	"testing"
)

var gossipDescriptors []gossip.Describer = []gossip.Describer{
	&gossip.Descriptor{"a", "1"},
	&gossip.Descriptor{"b", "2"},
	&gossip.Descriptor{"c", "3"},
	&gossip.Descriptor{"d", "4"},
	&gossip.Descriptor{"e", "5"},
}

// sorted by increasing age
var cyDescriptors []gossip.Describer = []gossip.Describer{
	&Descriptor{&gossip.Descriptor{"f", "6"}, 0},
	&Descriptor{&gossip.Descriptor{"g", "7"}, 1},
	&Descriptor{&gossip.Descriptor{"h", "8"}, 2},
	&Descriptor{&gossip.Descriptor{"i", "9"}, 3},
	&Descriptor{&gossip.Descriptor{"j", "10"}, 4},
}

// Utility function
func viewEqualsList(V gossip.Viewer, descriptors []gossip.Describer) error {
	var ok bool
	var v *View
	if v, ok = V.(*View); !ok {
		return fmt.Errorf("gossip.Viewer is not a *cyclon.View.\n")
	}

	if len(v.View) != len(descriptors) {
		return fmt.Errorf("View and descriptors are not the same length:\nV: %v\ndescriptors: %v\n",
			v, descriptors)
	}

	for _, desc := range descriptors {
		found := false
		for key, vItem := range v.View {
			if key == desc.ID() && vItem.Equals(desc) {
				found = true

				var cyVItem *Descriptor
				if cyVItem, ok = vItem.(*Descriptor); !ok {
					return fmt.Errorf("%s of type %T is not a *cyclon.Descriptor.",
						vItem, vItem)
				}
				// if desc is also a cyclon.Descriptor (not mandatory)
				// compare ages
				var cyDesc *Descriptor
				if cyDesc, ok = desc.(*Descriptor); ok {
					if cyDesc.Age() != cyVItem.Age() {
						return fmt.Errorf("%s and %s are not the same age.",
							cyVItem, cyDesc)
					}
				}
				break
			}
		}

		if !found {
			return fmt.Errorf("v does not contain descriptor %s:\nV: %v\ndescriptors: %v\n",
				desc, v, descriptors)
		}
	}
	return nil
}

func TestViewNewViewGossip(t *testing.T) {
	V, err := NewView(gossipDescriptors...)

	if err != nil {
		t.Errorf("TestViewNewViewGossip failed: %v\n", err)
	}

	if err = viewEqualsList(V, gossipDescriptors); err != nil {
		t.Errorf("TestViewNewViewGossip failed: %v", err)
	}
}

func TestViewNewViewCyclon(t *testing.T) {
	V, err := NewView(cyDescriptors...)

	if err != nil {
		t.Errorf("TestViewNewViewCyclon failed: %v\n", err)
	}

	if err = viewEqualsList(V, cyDescriptors); err != nil {
		t.Errorf("TestViewNewViewCyclon failed: %v", err)
	}
}

func TestViewUnionViewDistinct(t *testing.T) {
	var V1, V2, V3 gossip.Viewer
	var err error
	V1, _ = NewView(gossipDescriptors...)
	V2, _ = NewView(cyDescriptors...)

	descriptors := append(gossipDescriptors, cyDescriptors...)
	V3, err = UnionView(V1, V2)
	if err != nil {
		t.Errorf("TestViewUnionViewDistinct failed: %v\n", err)
	}

	if err = viewEqualsList(V3, descriptors); err != nil {
		t.Errorf("TestViewUnionViewDistinct failed: %v", err)
	}
}

func TestViewUnionViewOverlapping(t *testing.T) {
	var V1, V2, V3 gossip.Viewer
	var err error
	V1, _ = NewView(gossipDescriptors...)
	V2, _ = NewView(gossipDescriptors...)

	V3, err = UnionView(V1, V2)
	if err != nil {
		t.Errorf("TestViewUnionViewOverlapping failed: %v\n", err)
	}

	if err = viewEqualsList(V3, gossipDescriptors); err != nil {
		t.Errorf("TestViewUnionViewOverlapping failed: %v", err)
	}
}

func TestViewAddNew(t *testing.T) {
	V, _ := NewView(cyDescriptors[:3]...)
	var err error

	if err = V.Add(cyDescriptors[3]); err != nil {
		t.Errorf("TestViewAddNew failed: %v\n", err)
	}

	if err = viewEqualsList(V, cyDescriptors[:4]); err != nil {
		t.Errorf("TestViewAddNew failed: %v", err)
	}
}

func TestViewAddYounger(t *testing.T) {
	V, _ := NewView(cyDescriptors...)
	var err error

	youngerDescriptor := cyDescriptors[len(cyDescriptors)-1].Copy()
	youngerDescriptor.(*Descriptor).ResetAge()
	if err = V.Add(youngerDescriptor); err != nil {
		t.Errorf("TestViewAddYounger failed: %v\n", err)
	}

	expectedDescriptors := make([]gossip.Describer, len(cyDescriptors))
	copy(expectedDescriptors, cyDescriptors)
	expectedDescriptors[len(cyDescriptors)-1] = youngerDescriptor

	if err = viewEqualsList(V, expectedDescriptors); err != nil {
		t.Errorf("TestViewAddYounger failed: %v", err)
	}
}

func TestViewAddOlder(t *testing.T) {
	V, _ := NewView(cyDescriptors...)
	var err error

	older := cyDescriptors[0].Copy()
	older.(*Descriptor).IncrementAge()
	if err = V.Add(older); err != nil {
		t.Errorf("TestViewAddOlder failed: %v\n", err)
	}

	if err = viewEqualsList(V, cyDescriptors); err != nil {
		t.Errorf("TestViewAddOlder failed: %v", err)
	}
}

func TestViewCopy(t *testing.T) {
	V, _ := NewView(cyDescriptors...)
	Vcopy := V.Copy()
	var err error

	if err = viewEqualsList(V, cyDescriptors); err != nil {
		t.Errorf("TestViewCopy failed with V: %v", err)
	}

	if err = viewEqualsList(Vcopy, cyDescriptors); err != nil {
		t.Errorf("TestViewCopy failed with Vcopy: %v", err)
	}

	// Compare addresses
	if V == Vcopy {
		t.Errorf("TestViewCopy failed: both views have the same address:V: %p, Vcopy: %p\n",
			V, Vcopy)
	}

	for _, item := range cyDescriptors {
		if V.(*View).View[item.ID()] == Vcopy.(*View).View[item.ID()] {
			t.Errorf(
				"TestViewCopy failed: descriptor %v (@ %p) has the same address in both views:V.",
				item, item)
		}
	}
}

////////// Age functionality //////////
func TestViewIncrementAge(t *testing.T) {
	V, _ := NewView(cyDescriptors...)
	V.IncrementAge()

	incrDescriptors := make([]gossip.Describer, len(cyDescriptors))
	copy(incrDescriptors, cyDescriptors)
	for key := range incrDescriptors {
		incrDescriptors[key].(*Descriptor).IncrementAge()
	}

	if err := viewEqualsList(V, incrDescriptors); err != nil {
		t.Errorf("TestViewIncrementAge failed: %v\n", err)
	}
}

func TestViewOldest(t *testing.T) {
	V, _ := NewView(cyDescriptors...)
	oldestInV := V.Oldest()
	// cyDescriptors is sorted by increasing age
	oldestInList := cyDescriptors[len(cyDescriptors)-1]

	if !oldestInV.Equals(oldestInList) {
		t.Errorf("TestViewOldest failed: Should have returned %s, but returned %s.\n",
			oldestInList, oldestInV)
	}
}

func TestViewResetAge(t *testing.T) {
	V, _ := NewView(cyDescriptors...)
	V.ResetAge()

	resetDescriptors := make([]gossip.Describer, len(cyDescriptors))
	copy(resetDescriptors, cyDescriptors)
	for key := range resetDescriptors {
		resetDescriptors[key].(*Descriptor).ResetAge()
	}

	if err := viewEqualsList(V, resetDescriptors); err != nil {
		t.Errorf("TestViewResetAge failed: %v\n", err)
	}
}
