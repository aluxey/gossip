package cyclon

import (
// "fmt"
// "gitlab.inria.fr/aluxey/gossip"
)

type NewDescriberFromIP func(string) Describer

type Describer interface {
	// Implements gossip.Describer
	Copy() interface{}
	Equals(interface{}) bool
	ID() string
	IP() string
	SetID(ID string)
	String() string

	Age() int
	IncrementAge()
	ResetAge()
	Younger(interface{}) bool
}

// // Descriptor implements the cyclon.Describer interface
// // It gives descriptor's age operations in addition to gossip.Descriptor functionality
// type Descriptor struct {
// 	gossip.Describer

// 	MyAge int
// }

// func NewDescriptor(IP string) (d *Descriptor) {
// 	return &Descriptor{gossip.NewDescriptor(IP), 0}
// }

// func NewDescriptorFromGossipDescriber(gd gossip.Describer) (d *Descriptor) {
// 	return &Descriptor{gd, 0}
// }

// func (d *Descriptor) Age() int { return d.MyAge }
// func (d *Descriptor) Copy() gossip.Describer {
// 	return &Descriptor{d.Describer.Copy(), d.MyAge}
// }
// func (d *Descriptor) IncrementAge() { d.MyAge++ }
// func (d *Descriptor) ResetAge()     { d.MyAge = 0 }
// func (d *Descriptor) String() string {
// 	return fmt.Sprintf("cyclon.Descriptor(%p){ID: %s, IP: %s, Age: %d}",
// 		d, d.ID(), d.IP(), d.MyAge)
// }
// func (d *Descriptor) Younger(d1 Describer) bool { return d.MyAge < d1.Age() }
