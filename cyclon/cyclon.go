package cyclon

import (
	"encoding/gob"
	"fmt"
	"gitlab.inria.fr/aluxey/csvlog"
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/transport"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"
)

const (
	DEBUG         = false
	CHAN_BUF_SIZE = 5
)

type Payload struct {
	Sender Describer
	V      Viewer
}

func init() {
	gob.Register(Payload{})
	// Do not forget the &
	// *View is a Viewer, not View
	gob.Register(&View{})
}

// Observer pattern to track answers
type Cyclon struct {
	// Me and the others
	myself Describer
	v      Viewer

	// Algorithm's parameters
	viewSize, gossipSize      int
	period, connectionTimeout time.Duration

	// Network
	tm           transport.Manager
	reqCh, ansCh <-chan transport.Message

	answerObservers struct {
		sync.RWMutex
		m map[string]chan Payload
	}

	// We use a map to avoid duplicate entries
	// receivers struct {
	// 	sync.RWMutex
	// 	m map[Receiver]struct{}
	// }

	bs gossip.Bootstrapper

	// running bool
	// stopCh chan struct{}

	// Functions to handle concrete types
	newDescriptor NewDescriberFromIP

	// Logging
	csv         *csvlog.CSVLog
	info, debug *log.Logger

	// Iteration counters
	cntActive, cntPassive int
	// Synchronization
	mutex sync.RWMutex
}

func New(myself Describer, newDescriptor NewDescriberFromIP,
	bs gossip.Bootstrapper, tm transport.Manager,
	period, viewSize, gossipSize int, logToCsv, debug bool) (*Cyclon, error) {
	cy := new(Cyclon)

	// Save cy.myself as a *cyclon.Descriptor depending on the parameter type
	// switch m := myself.(type) {
	// case Describer:
	// 	cy.myself = m
	// case gossip.Describer:
	// 	cy.myself = NewDescriptorFromGossipDescriber(m)
	// default:
	// 	return nil, fmt.Errorf(
	// 		"Parameter myself (%s) of unexpected type %T.",
	// 		myself, myself)
	// }

	cy.newDescriptor = newDescriptor

	//cy.running = false
	cy.bs = bs
	cy.cntActive, cy.cntPassive = 0, 0
	cy.gossipSize = gossipSize
	cy.myself = myself
	cy.period = time.Duration(period) * time.Millisecond
	cy.tm = tm
	cy.v = NewView()
	cy.viewSize = viewSize

	cy.connectionTimeout = cy.period
	// People register if they want to receive Cy's view when updated
	//cy.receivers.m = make(map[Receiver]struct{})

	// ----------- Network -----------
	reqCh := make(chan transport.Message)
	ansCh := make(chan transport.Message)
	// Register chans on the transport Manager
	cy.tm.RegisterObserver(transport.MessageType("CYCLON_REQ"), reqCh)
	cy.tm.RegisterObserver(transport.MessageType("CYCLON_ANS"), ansCh)
	// Keep a reference
	cy.reqCh = reqCh
	cy.ansCh = ansCh

	// Internal dispatcher
	cy.answerObservers.m = make(map[string]chan Payload)

	// ----------- Logging -----------
	// To console
	cy.info = log.New(os.Stdout, "CYCLON: ", log.Ltime|log.Lshortfile|log.Lmicroseconds)
	if debug {
		cy.debug = log.New(os.Stdout, "CYCLON: ", log.Ltime|log.Lshortfile|log.Lmicroseconds)
	} else {
		cy.debug = log.New(ioutil.Discard, "CYCLON: ", log.Ltime|log.Lshortfile|log.Lmicroseconds)
	}
	// To CSV
	if logToCsv {
		now := time.Now()
		filename := fmt.Sprintf("cyclon-%v_%02d-%02d-%02d_%02dh%02d.csv",
			cy.myself.ID(),
			now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
		header := []string{"id", "timestamp", "view"}

		var err error
		if cy.csv, err = csvlog.New(filename, header); err != nil {
			return nil, fmt.Errorf("Failed creating Cyclon CSVLog: %v", err)
		}
	}

	cy.info.Printf(
		"Cyclon initialized with viewSize=%d, gossipSize=%d and a period of %v.\n",
		viewSize, gossipSize, cy.period)

	return cy, nil
}

func (cy *Cyclon) Loop() {
	cy.debug.Printf("Loop started.\n")

	// cy.StopCh = make(chan struct{})
	// cy.running = true

	ticker := time.NewTicker(cy.period)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			go cy.active()
		case mess := <-cy.reqCh:
			go cy.passive(mess)
		case mess := <-cy.ansCh:
			go cy.dispatchAnswers(mess)
			// case <-cy.StopCh:
			// 	break
		}
	}
}

// Getter
func (cy *Cyclon) View() Viewer {
	cy.mutex.RLock()
	defer cy.mutex.RUnlock()
	return cy.v.Copy()
}

func (cy *Cyclon) active() {
	// We lock mutex while we use V or myself
	cy.mutex.Lock()
	// We create a local variable in case the counter gets incremented while out of mutex
	cnt := cy.cntActive
	cy.cntActive++

	// 1 - Increase by one the age of all neighbors
	cy.v.IncrementAge()

	// 2 - Select neighbor Q with the highest age among all neighbors ...
	var q Describer
	if cy.v.Len() == 0 {
		cy.debug.Printf(
			"[active #%v] Our view is empty, we query the bootstrap server.\n",
			cnt)
		if IP, err := cy.bs.GetIP(); err != nil {
			cy.info.Printf(
				"[active #%v] The bootstraper failed providing an IP: %v\n",
				cnt, err)
			cy.mutex.Unlock()
			return
		} else {
			cy.debug.Printf(
				"[active #%v] The bootstraper sent us the following IP: %v\n",
				cnt, IP)
			// q won't be added to Cyclon's view
			// We use the BaseDescriptor to create a gossip.Descriptor without other knowledge than the Addr
			q = cy.newDescriptor(IP)
		}
	} else {
		q = cy.v.Oldest()

		if q == nil {
			cy.info.Printf("[active #%d] View.Oldest() returned nil with view=%v\n", cnt, cy.v)
			cy.mutex.Unlock()
			return
		}
		cy.v.Remove(q)
	}

	// 2 - Select [...] l-1 other random neighbors
	buf_snd := cy.v.GetSubset(cy.gossipSize - 1)
	// 3 - Replace Q's entry w/ a new entry of age 0 and with P's address
	// That is: add my descriptor (has age of 0) to the view we will send
	buf_snd.Add(cy.myself)

	cy.debug.Printf(
		"[active #%v] Sending following CYCLON_REQ to %s:\n%v\n",
		cnt, q, buf_snd)

	// 4 - Send the updated subset to Q
	go transport.EncodeAndSend(
		Payload{cy.myself, buf_snd.Copy()},
		q.IP(), cy.tm, transport.MessageType("CYCLON_REQ"))

	cy.mutex.Unlock()

	select {
	case <-time.After(cy.connectionTimeout):
		cy.debug.Printf("[active #%v] %v timed out.\n", cnt, q)

		// q has already been removed from the view, no need to do it twice
		// cy.mutex.Lock()
		// cy.v.Remove(q)
		// cy.mutex.Unlock()

	// 5 - Receive from Q a subset of no more than l of its own entries
	case payload := <-cy.getChannel(q):
		cy.debug.Printf(
			"[active #%v] Received following CYCLON_ANS from %s:\n%v\n\n",
			cnt, payload.Sender.ID(), payload.V)

		cy.mergeWithView(payload, buf_snd, q)
	}
}

func (cy *Cyclon) passive(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("CYCLON_REQ"))

	cy.mutex.Lock()
	cy.cntPassive++

	if x == nil {
		cy.info.Printf("[passive #%v] Failed decoding payload.\n",
			cy.cntPassive)
		cy.mutex.Unlock()
		return
	}
	payload := x.(Payload)

	cy.debug.Printf("[passive #%v] Received following CYCLON_REQ from %s:\n%v\n",
		cy.cntPassive, payload.Sender, payload.V)

	// Upon reception, Q selects l of its own neighbors to send back to P
	buf_snd := cy.v.GetSubset(cy.gossipSize - 1)
	buf_snd.Add(cy.myself)
	// We remove P from the view (in paper at step 6, but doing so here
	// removes useless transport overload)
	buf_snd.Remove(payload.Sender)

	cy.debug.Printf("[passive #%v] Sending following CYCLON_ANS to %s:\n%v\n\n",
		cy.cntPassive, payload.Sender, buf_snd)

	go transport.EncodeAndSend(
		Payload{cy.myself, buf_snd.Copy()},
		payload.Sender.IP(), cy.tm, transport.MessageType("CYCLON_ANS"))

	cy.mutex.Unlock()

	cy.mergeWithView(payload, buf_snd, nil)
}

func (cy *Cyclon) dispatchAnswers(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("CYCLON_ANS"))
	if x == nil {
		cy.info.Printf(
			"[dispatchAnswers] Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	cy.answerObservers.RLock()
	if cy.answerObservers.m[payload.Sender.IP()] == nil {
		cy.debug.Printf(
			"[dispatchAnswers] Received answer from unexpected sender %v, skipping.\n",
			payload.Sender)
		cy.answerObservers.RUnlock()
		return
	}

	// non-blocking -channel has buffer of CHAN_BUF_SIZE
	cy.answerObservers.m[payload.Sender.IP()] <- payload
	cy.answerObservers.RUnlock()
}

func (cy *Cyclon) getChannel(d Describer) <-chan Payload {
	cy.answerObservers.Lock()
	defer cy.answerObservers.Unlock()

	if cy.answerObservers.m[d.IP()] == nil {
		cy.debug.Printf("[getChannel] Registering new channel for %v.\n",
			d)
		cy.answerObservers.m[d.IP()] = make(chan Payload, CHAN_BUF_SIZE)
	}

	return cy.answerObservers.m[d.IP()]
}
