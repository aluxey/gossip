package cyclon

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
)

type Viewer interface {
	Add(...Describer)
	Copy() Viewer
	Diff(Viewer)
	Equals(Viewer) bool
	GetSubset(int) Viewer
	IDs() string
	Len() int
	List() []Describer
	Map() map[string]Describer
	Remove(...Describer)

	IncrementAge()
	Oldest() Describer
	ResetAge()
}

// View implements Viewer
// It gives descriptor's age operation in addition to gossip.View functionality
type View map[string]Describer

func NewView(items ...Describer) Viewer {
	var v View = make(map[string]Describer)
	v.Add(items...)
	return &v
}

func (v *View) Add(items ...Describer) {
	for _, item := range items {
		if item == nil {
			continue
		}

		// Keep younger if item already in v
		if existing, ok := (*v)[item.ID()]; ok {
			if existing.Younger(item) {
				continue
			}
		}

		(*v)[item.ID()] = item.Copy().(Describer)
	}
}
func (v *View) Copy() Viewer {
	var v2 View = make(map[string]Describer)
	for key, item := range *v {
		v2[key] = item.Copy().(Describer)
	}
	return &v2
}
func (v *View) Diff(v2 Viewer) {
	if v2 == nil {
		return
	}
	for key2, d2 := range v2.Map() {
		// If duplicate
		if d, ok := (*v)[key2]; ok {
			// Keep younger
			if d2.Younger(d) {
				delete(*v, key2)
			}
		} else {
			delete(*v, key2)
		}
	}
}
func (v *View) Equals(v2 Viewer) bool {
	if v2 == nil || v.Len() != v2.Len() {
		return false
	}
	for _, item := range *v {
		if item2, ok := v2.Map()[item.ID()]; ok {
			if !item.Equals(item2) {
				return false
			}
		} else {
			return false
		}
	}
	return true
}
func (v *View) GetSubset(length int) Viewer {
	if v.Len() < length {
		return v.Copy()
	}

	return NewView(v.List()[:length]...)
}
func (v *View) IDs() string {
	ret := ""
	i := 0
	for key := range *v {
		ret += key
		if i < len(*v)-1 {
			ret += gossip.ID_SEPARATOR
		}
		i++
	}
	return ret
}
func (v *View) Len() int {
	return len(*v)
}
func (v *View) List() []Describer {
	list := make([]Describer, v.Len())
	i := 0
	for _, item := range *v {
		list[i] = item.Copy().(Describer)
		i++
	}
	return list
}
func (v *View) Map() map[string]Describer {
	newMap := make(map[string]Describer)
	for id, item := range *v {
		newMap[id] = item.Copy().(Describer)
	}
	return newMap
}
func (v *View) Remove(items ...Describer) {
	for _, item := range items {
		if item != nil {
			delete(*v, item.ID())
		}

	}
}
func (v *View) String() string {
	ret := fmt.Sprintf("cyclon.View(len:%v){", v.Len())
	for k, item := range *v {
		ret += fmt.Sprintf("\n\t%s -> %s,", k, item)
	}
	ret += "\n}"
	return ret
}

// ----------------- Age functionality -----------------
func (v *View) IncrementAge() {
	for k := range *v {
		(*v)[k].IncrementAge()
	}
}
func (v *View) Oldest() Describer {
	oldestAge := 0
	oldestKey := ""
	for k, item := range *v {
		// First give a value to oldestKey
		if oldestKey == "" {
			oldestKey = k
			oldestAge = item.Age()
			// In next iterations, get oldest descriptor
		} else if item.Age() > oldestAge {
			oldestKey = k
			oldestAge = item.Age()
		}
	}

	if oldestKey == "" {
		return nil
	}

	return (*v)[oldestKey]
}
func (v *View) ResetAge() {
	for k := range *v {
		(*v)[k].ResetAge()
	}
}

// type View struct {
// 	gossip.Viewer
// }

// func NewView(items ...gossip.Describer) (Viewer, error) {
// 	gossipView, err := gossip.NewView(items...)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &View{gossipView}, nil

// 	// v := new(View)
// 	// v.View = NewView(items)
// 	// if err := v.Add(items...); err != nil {
// 	// 	return nil, err
// 	// }
// 	// return v, nil
// }

// // ----------------- Age functionality -----------------
// func (v *View) IncrementAge() {
// 	for _, item := range v.List() {
// 		if cyItem, ok := item.(Describer); !ok {
// 			panic(fmt.Sprintf(
// 				"cyclon.View contains an item that is not an cyclon.Describer: %v of type %T",
// 				item, item))
// 		} else {
// 			cyItem.IncrementAge()
// 		}

// 	}
// }
// func (v *View) Oldest() Describer {
// 	oldestAge := 0
// 	oldestKey := ""
// 	for _, item := range v.List() {
// 		if cyItem, ok := item.(Describer); !ok {
// 			panic(fmt.Sprintf(
// 				"cyclon.View contains an item that is not a cyclon.Describer: %v of type %T",
// 				item, item))
// 		} else {
// 			if cyItem.Age() > oldestAge {
// 				oldestKey = item.ID()
// 				oldestAge = cyItem.Age()
// 			}
// 		}
// 	}
// 	return v.Get(oldestKey).(Describer)
// }
// func (v *View) ResetAge() {
// 	for _, item := range v.List() {
// 		if cyItem, ok := v.Get(item.ID()).(Describer); cyItem != nil {
// 			panic(fmt.Sprintf(
// 				"cyclon.View contains an item that is not a *cyclon.Descriptor: %v of type %T",
// 				cyItem, cyItem))
// 		} else {
// 			cyItem.ResetAge()
// 		}

// 	}
// }

// // ----------------- Overrides from gossip.View -----------------
// // func UnionView(views ...gossip.Viewer) (Viewer, error) {
// // 	v, _ := gossip.NewView()
// // 	for _, view := range views {
// // 		if view == nil {
// // 			continue
// // 		}
// // 		if err := v.Add(view.List()...); err != nil {
// // 			return nil, err
// // 		}
// // 	}

// // 	return v, nil
// // }

// func (v *View) Add(items ...gossip.Describer) error {
// 	var ok bool
// 	for _, item := range items {
// 		if item == nil {
// 			continue
// 		}

// 		// Type assertion/cast for parameter item

// 		var toAdd Describer
// 		if toAdd, ok = item.(Describer); !ok {
// 			toAdd = NewDescriptorFromGossipDescriber(item)
// 		}

// 		// When duplicates...
// 		var d gossip.Describer
// 		if d = v.Get(toAdd.ID()); d != nil {
// 			// Type assertion/cast for internal item
// 			var existing Describer
// 			if existing, ok = d.(Describer); !ok {
// 				panic(fmt.Sprintf(
// 					"Value %v of type %T should be of type cyclon.Describer",
// 					existing, existing))
// 			}

// 			// ...keep younger
// 			if existing.Younger(toAdd) {
// 				continue
// 			}
// 		}
// 		v.Add(toAdd)
// 	}
// 	return nil
// }

// // Overriding gossip.View to keep cyclon functionality when copying
// func (v *View) Copy() gossip.Viewer {
// 	v2, _ := NewView(v.List()...)
// 	return v2
// }

// // Must be overridden to use cyclon.View.Add in cyclon.View.Merge
// func (v *View) Merge(views ...gossip.Viewer) error {
// 	for _, view := range views {
// 		if err := v.Add(view.List()...); err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }
// func (v *View) String() string {
// 	ret := fmt.Sprintf("View(len:%v){", v.Len())
// 	for _, item := range v.List() {
// 		if d, ok := item.(Describer); !ok {
// 			panic(fmt.Sprintf(
// 				"cyclon.View contains an item that is not a *cyclon.Descriptor: %v of type %T",
// 				item, item))
// 		} else {
// 			ret += fmt.Sprintf("\n\t%s,", d)
// 		}

// 	}
// 	ret += "\n}"
// 	return ret
// }
