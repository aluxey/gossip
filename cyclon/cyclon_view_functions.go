package cyclon

import (
	"gitlab.inria.fr/aluxey/gossip"
	"time"
)

// mergeAmongReplaceable: Merge v2 into v up to maxLength items,
// 	removing entries among repl to make room for v2's items.
//
// v: 			pointer to View, the View we are replacing into
// v2: 			pointer to View, the View we are replacing from
// repl: 		pointer to View, the items in v available for replacement
// maxLength: 	the desired final length of v
func mergeAmongReplaceable(v, v2, repl Viewer, maxLength int) {
	// First make room into the output view by removing the replaceable elements
	v.Remove(repl.List()...)

	// Then add items from the input view v2
	for _, item := range v2.List() {
		if v.Len() >= maxLength {
			break
		}

		// cyclon.View.Add keeps younger items when it sees duplicates
		v.Add(item)
	}
	// Finally, fill remaining place in v with items from repl
	for _, item := range repl.List() {
		if v.Len() >= maxLength {
			break
		}

		v.Add(item)
	}
}

func (cy *Cyclon) mergeWithView(payload Payload, repl Viewer, q Describer) {
	// 5 - Receive FROM Q ...
	// if q != nil && q.ID() != payload.Sender.ID() {
	// 	cy.info.Printf(
	// 		"[mergeWithView] We expected a datagram from %v, but it came from %v!",
	// 		q.ID(), payload.Sender.ID())
	// }

	// 5 - ... a subset of NO MORE THAN L of its own entries
	// v := payload.V
	// if v.Len() > cy.gossipSize {
	// 	cy.info.Printf(
	// 		"[mergeWithView] The received view (len: %v) is bigger than gossip size (%v), it shouldn't be!\n",
	// 		v.Len(), cy.gossipSize)
	// }

	cy.mutex.Lock()
	defer cy.mutex.Unlock()
	// 6 - Discard entries pointing at P ...
	//v.Remove(cy.myself) // Done in passive, before sending
	// ... and entries already contained in P's cache
	payload.V.Diff(cy.v)

	// Save previous state of our View
	// prevView := cy.cloneView(cy.v)

	// 7 - Update P's cache to include all remaining entries,
	// by firstly using empty cache slots (if any),
	// and secondly replacing entries among the ones sent to Q
	mergeAmongReplaceable(cy.v, payload.V, repl, cy.viewSize)

	// Remove myself if was sent to me
	cy.v.Remove(cy.myself)

	// If the view has changed, send the new one to the Receivers
	//cy.updateReceivers(prevView)

	// Log updates to CSV
	if cy.csv != nil {
		// header: {"Profile", "Timestamp", "View"}
		cy.csv.Write([]string{
			cy.myself.ID(),
			time.Now().Format(gossip.TIME_LAYOUT),
			cy.v.IDs()})
	}

	cy.debug.Printf(
		"[mergeWithView] Successfully merged! New view is:\n%v\n\n",
		cy.v)
}
