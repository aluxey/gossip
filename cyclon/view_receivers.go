package cyclon

// import "gitlab.inria.fr/aluxey/gossip"

// /*
// Receivers implement the ReceiveCyclonUpdate(*View) method and register to Cyclon.

// Cyclon pushes its view every time it is updated, by calling its updateReceivers(*View) method.
// The calling function needs to provide the previous view as a parameter for comparison before sending updates.
// */

// type Receiver interface {
// 	// Send a copy!
// 	ReceiveCyclonUpdate(gossip.Viewer)
// }

// func (cy *Cyclon) AddReceiver(vr Receiver) {
// 	// cy.Receivers modifications must be done inside the mutex
// 	cy.receivers.Lock()
// 	defer cy.receivers.Unlock()

// 	cy.receivers.m[vr] = struct{}{}
// }

// // This may be useful in the future, but not now
// //func (cy *Cyclon) RemoveReceiver(vr Receiver) {}

// func (cy *Cyclon) updateReceivers(prevView gossip.Viewer) {
// 	// Must be called from within the mutex!

// 	if prevView.Equals(cy.v) {
// 		return
// 	}

// 	cy.debug.Printf("[updateReceivers] The view has changed: " +
// 		"we update the Receivers.\n")

// 	cy.receivers.RLock()
// 	for vr := range cy.receivers.m {
// 		go vr.ReceiveCyclonUpdate(cy.v.Copy())
// 	}
// 	cy.receivers.RUnlock()
// }
