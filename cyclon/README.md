# Cyclon - A Random Peer Sampling algorithm

Ramdom Peer Sampling (RPS) algorithms are a type of gossip algorithms.
Their purpose is to provide a peer participating in a distributed algorithm 
with a constantly evolving random sample of the other participants' 
descriptors.

Cyclon is an RPS implementation, you can read more about it here:

* [*CYCLON: Inexpensive Membership Management for Unstructured P2P Overlays*](http://www.csl.mtu.edu/cs6461/www/Reading/Voulgaris-jnsm05.pdf) by Spyros Voulgaris, Daniela Gavidia, and Maarten van Steen in the Journal of Network and Systems Management, Vol. 13, No. 2, June 2005.


## Install

	go get gitlab.inria.fr/aluxey/gossip/cyclon

## Usage

Create a Cyclon instance with

	New(myself gossip.Describer, bs gossip.Bootstrapper, tm transport.Manager,
	period, viewSize, gossipSize int, logToCsv, debug bool) (*Cyclon, error) 

Cyclon requires a `gossip.Describer` (e.g. `gossip.Descriptor` or `cyclon.Descriptor`) containing at least your IP address; a `gossip.Bootstrapper` (e.g. `gossip.ListBootstrap`) containing 0 or more IP addresses of other peers; a `transport.Manager` (e.g. `transport.TCPManager` or `transport.UDPManager` (beware not to exceed the MTU)) that will handle network communications, and some parameters:

* `period` (in s) is the period at which this Cyclon node will try to exchange views with his fellow peers;
* `viewSize` is the maximum size of Cyclon's view;
* `gossipSize` is the maximum size of the exchanged views.
* `logToCsv` will dictate whether you want to report the view's updates to a CSV file;
* `debug` will enable verbose output to stdout.

The Cyclon instance will not start until you call 

	(cy *Cyclon) Loop()

Then, as I guess you want to use Cyclon's view, clients can register to receive it whenever it is updated:

	(cy *Cyclon) AddReceiver(vr Receiver)

The `Receiver` is your client struct. It needs to implement the `Receiver` interface:

	type Receiver interface {
		ReceiveCyclonUpdate(gossip.Viewer)
	}

`YourClient.ReceiveCyclonUpdate(gossip.Viewer)` will be called every time Cyclon has updated its view.

The package also contains a `Descriptor` and a `View` structs that resp. implement `gossip.Describer` and `gossip.Descriptor` with the addition of the descriptor's age, but this is internal cooking.

## In action 

You can see Cyclon in action as I tested it in the [cyclon-test](https://gitlab.inria.fr/aluxey/cyclon-test) repo. 
As you will see it there, it satisfies the randomness properties that are expected.

----

By Adrien Luxey in June 2018.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.